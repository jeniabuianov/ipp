package FactoryMethod;

public class ShopFactory {
    public Shop getShop(String name){
        if(name == null){
            return null;
        }
        name = name.toUpperCase();

        if(name.equalsIgnoreCase("ATRIUM")){
            return new Atrium();

        } else if(name.equalsIgnoreCase("JUMBO")){
            return new Jumbo();

        } else if(name.equalsIgnoreCase("ELAT")){
            return new Elat();
        }

        return null;
    }
}
