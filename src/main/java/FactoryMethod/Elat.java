package FactoryMethod;

import java.util.ArrayList;

public class Elat implements Shop {

    private String name;
    private ArrayList<String> magazines;
    {
        magazines = new ArrayList<String>();
        magazines.add("Clothes");
        magazines.add("Shoes");
    }

    Elat(){
        this.name = this.getClass().getName();
    }

    @Override
    public void displayInfo() {
        System.out.println(name+"\n");
        for (int i=0;i<magazines.size();i++)
            System.out.println("Magazine: "+magazines.get(i));
    }
}
