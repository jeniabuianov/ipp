package FactoryMethod;

public class FactoryMain {

    public static void main(String[] args){
        ShopFactory factory = new ShopFactory();

        Shop shop1 = factory.getShop("atrium");
        shop1.displayInfo();

        Shop shop2 = factory.getShop("jumbo");
        shop2.displayInfo();

        Shop shop3 = factory.getShop("elat");
        shop3.displayInfo();

    }
}
