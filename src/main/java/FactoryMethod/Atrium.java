package FactoryMethod;

import java.util.ArrayList;

public class Atrium implements Shop {

    private String name;
    private ArrayList<String> magazines;
    {
        magazines = new ArrayList<String>();
        magazines.add("Clothes");
        magazines.add("Adventure");
        magazines.add("Books");
        magazines.add("Shoes");
        magazines.add("Pizza");
        magazines.add("Cafe");
    }

    Atrium(){
        this.name = this.getClass().getName();
    }

    @Override
    public void displayInfo() {
        System.out.println(name+"\n");
        for (int i=0;i<magazines.size();i++)
            System.out.println("Magazine: "+magazines.get(i));
        System.out.println("\n");
    }
}
