package Prototype;

public class Elat implements Shop {
    private String name;
    private String director;
    private int numberFloors;
    private int[] availableFloors;

    public Elat(){
        this.name = "Elat CC";
        this.director = "Alex Alex";
        this.numberFloors = 2;
        availableFloors = new int[2];
        availableFloors[0] = 1;
        availableFloors[1] = 3;
    }

    public Elat(String name, String director, int numberFloors, int startFloor, int endFloor){
        this.name = name;
        this.director = director;
        this.numberFloors = numberFloors;
        this.availableFloors = new int[2];
        availableFloors[0] = startFloor;
        availableFloors[1] = endFloor;
    }

    public Elat(Elat obj){
        if (obj != null){
            this.numberFloors = obj.numberFloors;
            this.director = obj.director;
            this.name = obj.name;
            this.availableFloors = obj.availableFloors;
        }
    }


    @Override
    public Shop clone() {
        return new Elat(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDirector() {
        return director;
    }

    @Override
    public int getNumberFloors() {
        return numberFloors;
    }

    @Override
    public int[] getAvailableFloors() {
        return availableFloors;
    }

    @Override
    public void show() {
        System.out.println(name+"\n"+"Director: "+director);
        System.out.println("Floors: "+numberFloors);
        System.out.println("Available floors: "+availableFloors[0]+"-"+availableFloors[1]);
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Elat)) return false;
        Elat o = (Elat) object;
        return o.getNumberFloors()==numberFloors && o.getDirector().equals(getDirector()) && o.getName().equals(getName());
    }
}
