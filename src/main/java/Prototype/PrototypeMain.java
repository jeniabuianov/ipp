package Prototype;

import java.util.ArrayList;
import java.util.List;

public class PrototypeMain {
    public static void main(String[] args){
        List<Shop> shops = new ArrayList<>();

        shops.add(new Atrium());
        shops.add(new Atrium("ATRIUM!","Igor Dodon",10,5,8));
        shops.add(shops.get(1).clone());
        shops.add(new Jumbo());
        shops.add(new Elat());
        shops.add(shops.get(4).clone());
        shops.add(new Elat("Elat!","Igor Dodon",10,5,8));
        shops.add(shops.get(6).clone());

        for(int i=0;i<shops.size();i++){
            shops.get(i).show();
            System.out.println("--------------------\n");

            for(int j=0;j<i;j++){
                boolean equals = shops.get(i).equals(shops.get(j));
                if (equals){
                    System.out.println("\t\tElement "+(i+1)+" is copy of "+j);
                    System.out.println("--------------------\n");
                }
            }
        }
    }
}
