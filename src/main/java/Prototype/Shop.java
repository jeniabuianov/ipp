package Prototype;

public interface Shop {
    Shop clone();
    String getName();
    String getDirector();
    int getNumberFloors();
    int[] getAvailableFloors();
    boolean equals(Object obj);
    void show();
}
