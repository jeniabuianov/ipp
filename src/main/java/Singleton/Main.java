package Singleton;

import java.util.Scanner;

public class Main {
    private static String shop1;
    private static String shop2;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter name for first shop:");
        shop1 = scan.nextLine();

        System.out.println("Please enter name for second shop:");
        shop2 = scan.nextLine();

        Singleton Shop1 = Singleton.getInstance(shop1);
        Singleton Shop2 = Singleton.getInstance(shop2);

        System.out.println("The result of calling singleton.name should be the same:");
        System.out.println(Shop1.name);
        System.out.println(Shop2.name);
    }
}
