package Builder;

import java.util.ArrayList;
import java.util.HashMap;

public class Mall implements ShopBuilder {
    private Shop  shop = new Shop("Mall");
    @Override
    public void buildName() {
        shop.setName("Mall");
    }

    @Override
    public void buildCategories() {
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Restaurants");
        categories.add("Clothes");
        categories.add("Meal");
        categories.add("Shoes");
        shop.setCategories(categories);
    }

    @Override
    public void buildRestaurans() {
        ArrayList<String> restaurants = new ArrayList<String>();
        restaurants.add("KFC");
        restaurants.add("McDonald's");
        restaurants.add("Beef House");
        restaurants.add("Star Kebab");
        restaurants.add("Blinoff");
        shop.setRestaurants(restaurants);
    }

    @Override
    public void buildFloors() {
        HashMap<Integer,String> floors = new HashMap<Integer, String>();
        floors.put(1,"KFC");
        floors.put(2,"McDonald's");
        floors.put(3,"Beef House");
        floors.put(4,"Blinoff");
        floors.put(5,"LC Wikiki");
        floors.put(6,"De Facto");
        floors.put(7,"Colin");
        floors.put(8,"Fourchette");

        shop.setFloors(floors);
    }

    public Shop getShop() {
        return shop;
    }
}
