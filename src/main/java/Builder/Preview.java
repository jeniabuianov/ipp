package Builder;

public class Preview {
    ShopBuilder shopBuilder;

    public Preview(ShopBuilder shop){
        shopBuilder = shop;
    }

    public void setData(){
        shopBuilder.buildCategories();
        shopBuilder.buildName();
        shopBuilder.buildFloors();
        shopBuilder.buildRestaurans();
    }
}
