package Builder;

import com.sun.org.apache.bcel.internal.generic.INEG;

import java.util.ArrayList;
import java.util.HashMap;

public class Atrium implements ShopBuilder {
    private Shop  shop = new Shop("Atrium");
    @Override
    public void buildName() {
        shop.setName("Atrium");
    }

    @Override
    public void buildCategories() {
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Clothes");
        categories.add("Restaurants");
        categories.add("Offices");
        categories.add("Meal");
        shop.setCategories(categories);
    }

    @Override
    public void buildRestaurans() {
        ArrayList<String> restaurants = new ArrayList<String>();
        restaurants.add("Midpoint");
        restaurants.add("Asian");
        restaurants.add("Cofee Bean");
        shop.setRestaurants(restaurants);
    }

    @Override
    public void buildFloors() {
        HashMap<Integer,String> floors = new HashMap<Integer, String>();
        floors.put(1,"Meal");
        floors.put(2,"Clothes");
        floors.put(3,"Restaurants");
        floors.put(4,"Offices");
        floors.put(5,"Offices");
        floors.put(6,"Offices");
        floors.put(7,"Offices");
        floors.put(8,"Offices");
        floors.put(9,"Offices");
        floors.put(10,"Offices");

        shop.setFloors(floors);
    }

    public Shop getShop() {
        return shop;
    }
}
