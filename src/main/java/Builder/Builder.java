package Builder;

public class Builder {
    public static void main(String[] args) {

        ShopBuilder sb = new Atrium();
        Shop atriumShop = sb.getShop();
        Preview preview = new Preview(sb);
        preview.setData();
        System.out.println(atriumShop);

        sb = new Mall();
        Shop mallShop = sb.getShop();
        preview = new Preview(sb);
        preview.setData();
        System.out.println("\n\n\n");
        System.out.println(mallShop);
    }
}
