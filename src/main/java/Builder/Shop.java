package Builder;

import java.util.ArrayList;
import java.util.HashMap;

public class Shop {
    private String name;
    private ArrayList<String> categories;
    private ArrayList<String> restaurants;
    private HashMap<Integer,String>  floors;

    public Shop(String name){
        this.name = name;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFloors(HashMap<Integer, String> floors) {
        this.floors = floors;
    }

    public void setRestaurants(ArrayList<String> restaurants) {
        this.restaurants = restaurants;
    }

    @Override
    public String toString(){
        StringBuilder string = new StringBuilder();
        string.append(name.toUpperCase()+"\n");
        string.append("\t\tCategories:\n\n");
        for (int i=0;i<categories.size();i++)
            string.append("\t\t\t"+categories.get(i)+"\n");
        string.append("\n\t\tRestaurants:\n");
        for (int i=0;i<restaurants.size();i++)
            string.append("\n\t\t\t"+restaurants.get(i));
        string.append("\n\n\t\tFloors:\n");
        for (int floor:floors.keySet()){
            string.append("\n\t\t\t"+floor+".\t\t"+floors.get(floor));
        }

        return string.toString();
    }
}
