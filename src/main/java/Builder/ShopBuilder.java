package Builder;

public interface ShopBuilder {
    void buildName();
    void buildCategories();
    void buildRestaurans();
    void buildFloors();
    Shop getShop();
}
