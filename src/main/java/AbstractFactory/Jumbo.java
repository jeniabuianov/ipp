package AbstractFactory;

public class Jumbo implements Shop {
    @Override
    public Magazin selectMagazin() {
        return new JumboMagazines();
    }

    @Override
    public Order createOrder() {
        return new GenerateOrder(this.getClass().getName().substring(16));
    }
}
