package AbstractFactory;

public class Atrium implements Shop {
    @Override
    public Magazin selectMagazin() {
        return new AtriumMagazines();
    }

    @Override
    public Order createOrder() {
        return new GenerateOrder(this.getClass().getName().substring(16));
    }
}
