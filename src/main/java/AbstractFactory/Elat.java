package AbstractFactory;

public class Elat implements Shop {
    @Override
    public Magazin selectMagazin() {
        return new ElatMagazines();
    }

    @Override
    public Order createOrder() {
        return new GenerateOrder(this.getClass().getName().substring(16));
    }
}
