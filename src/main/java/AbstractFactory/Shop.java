package AbstractFactory;

interface Shop {
    public abstract Magazin selectMagazin();
    public abstract Order createOrder();
}
