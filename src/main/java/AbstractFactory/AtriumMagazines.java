package AbstractFactory;

import java.util.ArrayList;

public class AtriumMagazines implements Magazin {
    private ArrayList<String> magazines;
    {
        magazines = new ArrayList<String>();
        magazines.add("Clothes");
        magazines.add("Juvelire");
        magazines.add("Adventure");
        magazines.add("Books");
        magazines.add("Shoes");
        magazines.add("Pizza");
        magazines.add("Cafe");
        magazines.add("Internet");
    }

    @Override
    public ArrayList<String> showMagazines() {
        for (int i=0;i<magazines.size();i++)
            System.out.println(magazines.get(i).trim()+" \t\t\t"+(i+1)+" floor");
        return magazines;
    }
}
