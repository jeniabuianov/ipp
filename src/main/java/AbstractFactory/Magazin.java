package AbstractFactory;

import java.util.ArrayList;

public interface Magazin {
    public abstract ArrayList<String> showMagazines();
}
