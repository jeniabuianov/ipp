package AbstractFactory;

import java.util.UUID;

public class GenerateOrder implements Order {
    private String id;
    private String shop;

    GenerateOrder(String shop){
        this.shop = shop;
        this.generate();
    }

    private void generate(){
        this.id =  this.shop+" #"+UUID.randomUUID().toString();
    }

    @Override
    public String getId() {
        return this.id;
    }
}
