package AbstractFactory;

public interface Order {
    public abstract String getId();
}
