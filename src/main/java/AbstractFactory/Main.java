package AbstractFactory;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please select Shop:");
        System.out.println("1.\tAtrium");
        System.out.println("2.\tJumbo");
        System.out.println("3.\tElat");

        int in = scanner.nextInt();

        Shop shop = null;

        switch (in) {
            case 1:
                shop = new Atrium();
                break;
            case 2:
                shop = new Jumbo();
                break;
            case 3:
                shop = new Elat();
                break;
            default:
                System.out.println("Something went wrong!");
        }

        System.out.println("Generated ID for your purchases: "+shop.createOrder().getId());
        System.out.println("Allowed magazines categories for "+shop.getClass().getName().substring(16));
        ArrayList<String> magazines = shop.selectMagazin().showMagazines();
    }
}